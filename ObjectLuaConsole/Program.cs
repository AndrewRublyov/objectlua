﻿using System;
using System.IO;
using ObjectLua;


namespace ObjectLuaConsole
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine ("ObjectLua 1.0.0 Console\n");
			Machine m = new Machine (true);
			m.Execute (File.ReadAllText("script.olua"));

			while (true) {
				Console.Write ("> ");
				m.DoString (Console.ReadLine ());
			}
		}
	}
}
