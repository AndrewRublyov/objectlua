﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua.ValueTypes
{
    class StringValue : Value
    {
        private string value;

        public StringValue(string value)
        {
            this.value = value;
        }
        public float AsFloat()
        {
            try
            {
                return Convert.ToSingle(value);
            }
            catch (FormatException)
            {
                return 1;
            }
        }

        public string AsString()
        {
            return value;
        }

        public override string ToString()
        {
            return AsString();
        }
    }
}
