﻿using System;
using System.Collections.Generic;
using ObjectLua.AST;

namespace ObjectLua.ValueTypes
{
	public class Function : Value
	{
		private Func<List<Expression>, Value> func;

		public Function(Func<List<Expression>, Value> func)
		{
			this.func = func;
		}

	    public Function()
	    {
	        
	    }

		public virtual Value Execute(params Expression[] args)
		{
            var a = new List<Expression>(args);
			return func.Invoke(a);
		}

		#region Value implementation

		public float AsFloat ()
		{
			throw new NotImplementedException ();
		}

		public string AsString ()
		{
			throw new NotImplementedException ();
		}

		#endregion
	}
}

