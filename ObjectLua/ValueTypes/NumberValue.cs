﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua.ValueTypes
{
    class NumberValue : Value
    {
        private float value;

        public NumberValue(float value)
        {
            this.value = value;
        }
        public float AsFloat()
        {
            return value;
        }

        public string AsString()
        {
            return value + "";
        }

        public override string ToString()
        {
            return AsString();
        }
    }
}
