﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace ObjectLua.ValueTypes
{
    public class TableValue : Value
    {
        private Dictionary<string, Value> items  = new Dictionary<string, Value>();

        public Value GetItem(string index)
        {
            List<string> levels = index.Split('.').ToList();
            if (levels.Count == 1 && items.ContainsKey(index))
                return items[index];
            if (items.ContainsKey(index) && items[index] is TableValue)
            {
                levels.RemoveAt(0); // Удаляем первый, т.к. это этот узел
                return items[string.Join(".", levels)];
            }
            return new NumberValue(0);
        }

        public void SetItem(string index, Value value)
        {
            List<string> levels = index.Split('.').ToList();
            string current = levels[0];
            if (!items.ContainsKey(levels[0]))
            {
                items.Add(current, null);
            }
            if (levels.Count == 1)
            {
                items[current] = value;
                return;
            }
            levels.RemoveAt(0); // Удаляем первый, т.к. это этот узел
            if (value is TableValue)
                (items[current] as TableValue).SetItem(string.Join(".", levels), value);
        }


        public float AsFloat()
        {
            return 0;
        }

        public string AsString()
        {
            return "";
        }
    }
}
