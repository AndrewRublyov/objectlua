﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectLua.AST;

namespace ObjectLua.ValueTypes
{
    class UserFunction : Function
    {
        private BlockStatement statements;
        private List<string> parameters = new List<string>(); 
        public UserFunction(BlockStatement statements, List<string> parameters)
        {
            this.statements = statements;
            this.parameters = parameters;
        }

        public UserFunction(BlockStatement statements)
        {
            this.statements = statements;
        }

        public void AddParameter(string name)
        {
            parameters.Add(name);
        }


        public override Value Execute(params Expression[] args)
        {
            for (int i=0; i<args.Length; i++)
            {
                statements.GetParent().GetView().Set(parameters[i], args[i].Evaluate(), true);
            }
            try
            {
                statements.Execute();
            }
            catch (ReturnStatement e)
            {
                return e.Expression.Evaluate();
            }
            return new NumberValue(0);
            // TODO Очистить память
        }
    }
}
