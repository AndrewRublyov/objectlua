﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua.ValueTypes
{
    public interface Value
    {
        float AsFloat();
        string AsString();
    }
}
