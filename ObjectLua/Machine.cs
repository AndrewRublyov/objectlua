﻿using ObjectLua.AST;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua
{
    public class Machine
    {
        public static Variables Globals = new Variables();
		private bool debugTokens = false;
        private Lexer lexer;
        private Parser parser;
        public static Random r = new Random();
        private BlockStatement parent;
        public Machine()
        {
        }

        public Machine(BlockStatement parent)
        {
            this.parent = parent;
        }

        public Machine(bool debugTokens) : this()
		{
			this.debugTokens = debugTokens;
		}

        public void Execute(string chunk)
        {
            lexer = new Lexer(chunk);
            List<Token> tokens = lexer.Tokenize();
			if (debugTokens)
	            foreach (Token t in tokens)
	            {
	                Console.WriteLine(t);
	            }

            parser = new Parser(tokens);
            if (parent != null)
                parser.SetParent(parent);
            Statement result = parser.Parse();
            result.Execute();
        }

        public void DoString(string chunk)
        {
            lexer = new Lexer(chunk);
            List<Token> tokens = lexer.Tokenize();
            if (debugTokens)
                foreach (Token t in tokens)
                {
                    Console.WriteLine(t);
                }
            parser.Dotokens(tokens).Execute();
        }
    }
}
