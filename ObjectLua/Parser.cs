﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectLua.AST;
using ObjectLua.ValueTypes;

namespace ObjectLua
{
    public class Parser
    {
        private Token EOF = new Token(TokenType.EOF, "");
        private List<Token> Tokens;
        private int Size;
        private int Position = 0;
        BlockStatement root = new BlockStatement();

        public void SetParent(BlockStatement parent)
        {
            root.SetParent(parent);
        }
        public Parser(List<Token> tokens)
        {
            Tokens = tokens;
            Size = tokens.Count;

            // Временно константы задаются тут
            root.Set("Pi", new NumberValue((float)Math.PI), true);
            Func<List<Expression>, Value> func = (a) =>
            {
                Console.WriteLine("Working! " + a[0].Evaluate().AsFloat());
                return new NumberValue(a[0].Evaluate().AsFloat() + 1);
            };
            root.Set("func", new Function(func), true);


            Func<List<Expression>, Value> dostring = (a) =>
            {
                Machine m = new Machine(root);
                m.Execute(a[0].Evaluate().AsString());
                return new NumberValue(0);
            };
            root.Set("dostring", new Function(dostring), true);
        }
        public Statement Dotokens(List<Token> tokens)
        {
            Tokens = tokens; // Удаляем EOF
            Size = Tokens.Count;
            Position = 0;
            return Parse();
        }
        public Statement Parse()
        {
            BlockStatement block = new BlockStatement();
            block.SetView(root.GetView());
            while (!Match(TokenType.EOF))
            {
                block.AddStatement(Statement(block));
            }
            return block;
        }
        private Statement Block(BlockStatement parent)
        {
            BlockStatement block = new BlockStatement();
            block.Name = "ROOT";
            block.SetParent(parent);
            while (Get(0).Type != TokenType.END && Get(0).Type != TokenType.ELSE)
            {
                Match(TokenType.END); // Пропускаем любой
                block.AddStatement(Statement(block));
            }
            return block;
        }
		private FunctionExpression Function(BlockStatement parent) {
			// Сейчас Get(0) это название функции
			string name = Consume(0).GetText();
			Consume(TokenType.LPAREN); // Пропускаем (
			FunctionExpression func = new FunctionExpression(name, parent);
			while (!Match(TokenType.RPAREN)) {
                Expression exp = Expression(parent);
                func.AddArgument(exp);
				Match(TokenType.COMMA);
			}
			return func;
		}
        private Statement WhileStatement(BlockStatement parent)
        {
            WhileStatement stat = new WhileStatement();
            stat.SetParent(parent);
            Expression condition = Expression(parent);
            Match(TokenType.DO);
            Statement statement = Block(stat);
            Match(TokenType.END);
            stat.Setup(condition, statement, stat);
            return stat;
        }
        private Statement UserFunctionStatement(BlockStatement parent, bool local)
        {
            UserFunctionStatement stat = new UserFunctionStatement();
            string name = Get(0).GetText();
            Next();
            List<string> p = new List<string>();
            Match(TokenType.LPAREN);
            while (true)
            {
                if (Get(0).GetType() == TokenType.COMMA)
                    Match(TokenType.COMMA);
                else if (Get(0).GetType() == TokenType.RPAREN)
                {
                    Match(TokenType.RPAREN);
                    break;
                }
                string n = Get(0).GetText();
                p.Add(n);
                Next();
            }
            BlockStatement statement = (BlockStatement)Block(parent);
            Match(TokenType.END);
            stat.Setup(name, p, statement, parent, local);
            return stat;
        }
        private Statement ForStatement(BlockStatement parent)
        {
            ForStatement f = new ForStatement();
            f.SetParent(parent);
            f.SetView(parent.GetView());
            AssignmentStatement assignmentStatement = (AssignmentStatement)AssignmentStatement(f, true);
            Match(TokenType.COMMA);
            Expression to = Expression(parent);
            Expression step = new NumberExpression(1); // По стандарту щаг +1 
            if (Get(0).GetType() == TokenType.COMMA) // Также указан шаг, применяем его
            {
                Match(TokenType.COMMA);
                step = Expression(parent);
            }
            Match(TokenType.DO);
            Statement statement = Block(f);
            Match(TokenType.END);
            f.Setup(assignmentStatement, to, step, statement, parent);
            return f;
        }
        private Statement Statement(BlockStatement parent)
        {
            if (Match(TokenType.PRINT))
            {
                return new PrintStatement(Expression(parent));
            }

            if (Match(TokenType.IF))
            {
                return IfElse(parent);
            }

            if (Match(TokenType.WHILE))
            {
                return WhileStatement(parent);
            }

            if (Match(TokenType.FOR))
            {
                return ForStatement(parent);
            }

            if (Match(TokenType.CONTINUE))
            {
                return new ContinueStatement();
            }

            if (Match(TokenType.BREAK))
            {
                return new BreakStatement();
            }

            if (Match(TokenType.RETURN))
            {
                return new ReturnStatement(Expression(parent));
            }

            if (Match(TokenType.LOCAL))
            {
                if (Get(0).GetType() == TokenType.FUNCTION)
                {
                    Match(TokenType.FUNCTION);
                    return UserFunctionStatement(parent, true);
                }
                return AssignmentStatement(parent, true);
            }

            if (Match(TokenType.FUNCTION))
            {
                return UserFunctionStatement(parent, false);
            }

            if (Get(0).GetType() == TokenType.WORD && Get(1).GetType() == TokenType.LPAREN)
            {
                return new FunctionStatement(Function(parent));
            }

            // Переходим операции приравнивания
            return AssignmentStatement(parent, false);
        }
        private Statement AssignmentStatement(BlockStatement parent, bool local)
        {
            // WORD EQ
            Token current = Get(0);

            if (current.GetType() == TokenType.WORD && IsAssignmentOperator(Get(1).GetType()))
            {
                if (Get(2).GetType() == TokenType.LBRACE)
                {
                    return TableAssignmentStatement(parent, local);
                }
                TokenType znak = Get(1).GetType();
                string variable = current.GetText();
                Next(); // пропускаем переменную
                Next(); // пропускаем слово
                var result = new AssignmentStatement(variable, Expression(parent), znak, parent, local);
                return result;
            }

            throw new Exception("Unknown operator "+current.GetType() + " " + current.GetText());
        }
        private Statement TableAssignmentStatement(BlockStatement parent, bool local)
        {
            TableAssignmentStatement tas = new TableAssignmentStatement();
            string name = Get(0).GetText();
            Match(TokenType.WORD);
            Match(TokenType.EQ);
            Match(TokenType.LBRACE);

            List<AssignmentStatement> statements = new List<AssignmentStatement>();
            int index = 0;
            //AssignmentStatement a = new AssignmentStatement(index+"", Expression(parent), TokenType.EQ, parent, true);
            while (true)
            {
                if (Get(0).GetType() == TokenType.COMMA)
                {
                    Next();
                    continue;
                }
                if (Get(0).GetType() == TokenType.RBRACE)
                {
                    Next();
                    break;
                }
                index++;
                Console.WriteLine(index);
                if (Get(1).GetType() == TokenType.EQ)
                {
                    AssignmentStatement a = (AssignmentStatement)AssignmentStatement(parent, true);
                    statements.Add(a);
                }
                else
                {
                    AssignmentStatement a = new AssignmentStatement(index + "", Expression(parent), TokenType.EQ, parent, true);
                    statements.Add(a);
                }
            }

            tas.Setup(name, parent, statements);
            return tas;
        }
        private bool IsAssignmentOperator(TokenType type)
        {
            switch (type)
            {
                case TokenType.EQ: return true;
                case TokenType.PLUSEQ: return true;
                case TokenType.MINUSEQ: return true;
                case TokenType.STAREQ: return true;
                case TokenType.SLASHEQ: return true;
                case TokenType.POWEQ: return true;
                default: return false;
            }
        }
        private Statement IfElse(BlockStatement parent)
        {
            IfStatement ifst = new IfStatement();
            Expression condition = Expression(parent);
            Statement ifStatement;

            Match(TokenType.THEN);
            ifStatement = Block(parent);

            //= Statement();
            Statement elseStatement;
            if (Match(TokenType.ELSE))
            {
                // Блок в любом случае, потому что end закрывающий
                elseStatement = Block(parent);
            }
            else
            {
                elseStatement = null;
            }
            Match(TokenType.END);
            ifst.Setup(condition, ifStatement, elseStatement);
            return ifst;
        }

        // Сначала вызывается Expression, затем Additive, затем Multiplicative (древо AST)
        private Expression Expression(BlockStatement parent)
        { 
            // Переходим к + и -
            return LogicalOr(parent);
        }
        private Expression LogicalOr(BlockStatement parent)
        {
            Expression result = LogicalAnd(parent);
            while (true)
            {
                if (Match(TokenType.OR))
                {
                    result = new ConditionalExpression(result, LogicalAnd(parent), ConditionalExpression.Operator.OR);
                }
                break;
            }
            return result;
        }
        private Expression LogicalAnd(BlockStatement parent)
        {
            Expression result = Condition(parent);
            while (true)
            {
                if (Match(TokenType.AND))
                {
                    result = new ConditionalExpression(result, Condition(parent), ConditionalExpression.Operator.AND);
                }
                break;
            }
            return result;

        }
        // Отвечает за условные выражения
        private Expression Condition(BlockStatement parent)
        {
            // Спускаемся вниз по древу
            Expression result = Additive(parent);

            while (true)
            {
                if (Match(TokenType.EQEQ))
                {
                    result = new ConditionalExpression(result, Additive(parent), ConditionalExpression.Operator.EQUALS);
                    continue;
                }
                else if (Match(TokenType.LT))
                {
                    result = new ConditionalExpression(result, Additive(parent), ConditionalExpression.Operator.LT);
                    continue;
                }
                else if (Match(TokenType.GT))
                {
                    result = new ConditionalExpression(result, Additive(parent), ConditionalExpression.Operator.GT);
                    continue;
                }
                else if (Match(TokenType.GTEQ))
                {
                    result = new ConditionalExpression(result, Additive(parent), ConditionalExpression.Operator.GTEQ);
                    continue;
                }
                else if (Match(TokenType.LTEQ))
                {
                    result = new ConditionalExpression(result, Additive(parent), ConditionalExpression.Operator.LTEQ);
                    continue;
                }
                else if (Match(TokenType.EXCLEQ))
                {
                    result = new ConditionalExpression(result, Additive(parent), ConditionalExpression.Operator.NOT_EQUALS);
                    continue;
                }

                break;
            }

            return result;
        }
        // Отвечает за + и -
        private Expression Additive(BlockStatement parent)
        {
            // Спускаемся вниз по древу
            Expression result = Multiplicative(parent);

            while (true)
            {
                if (Match(TokenType.PLUS))
                {
                    result = new BinaryExpression(result, Multiplicative(parent), BinaryExpression.Operator.PLUS);
                    continue;
                }
                else if (Match(TokenType.MINUS))
                {
                    result = new BinaryExpression(result, Multiplicative(parent), BinaryExpression.Operator.MINUS);
                    continue;
                }
                else if (Match(TokenType.DOTDOT)) // Конкантинация
                {
                    result = new BinaryExpression(result, Multiplicative(parent), BinaryExpression.Operator.DOTDOT);
                    continue;
                }
                break;
            }

            return result;
        }
        // Умножение / деление
        private Expression Multiplicative(BlockStatement parent)
        {
            // Спускаемся вниз по древу
            Expression result = Pow(parent);

            while (true)
            {
                if (Match(TokenType.STAR))
                {
					result = new BinaryExpression(result, Pow(parent), BinaryExpression.Operator.STAR);
                    continue;
                }
                else if (Match(TokenType.SLASH))
                {
					result = new BinaryExpression(result, Pow(parent), BinaryExpression.Operator.SLASH);
                    continue;
                }
                break;
            }

            return result;
        }
		private Expression Pow(BlockStatement parent)
		{
			// Спускаемся вниз по древу
			Expression result = Unary(parent);

			while (true)
			{
				if (Match(TokenType.POW))
				{
					result = new BinaryExpression(result, Unary(parent), BinaryExpression.Operator.POW);
					continue;
				}
				break;
			}

			return result;
		}
        // Унарная операция (-5, -3)
        private Expression Unary(BlockStatement parent)
        {
            if (Match(TokenType.PLUS))
            {
                return Primary(parent);
            }
            else if (Match(TokenType.MINUS))
            {
                return new UnaryExpression(Primary(parent), UnaryExpression.Operator.MINUS);
            }
            else if (Match(TokenType.EXCL))
            {
                return new UnaryExpression(Primary(parent), UnaryExpression.Operator.NOT_EQUALS);
            }
            return Primary(parent);
        }
        // Операция чисел
        private Expression Primary(BlockStatement parent)
        {
            Token current = Get(0);
            if (Match(TokenType.NUMBER))
            {
                return new NumberExpression(Convert.ToSingle(current.Text));
            }
            if (Match(TokenType.LPAREN))
            {
                Expression result = Expression(parent);
                Match(TokenType.RPAREN);
                return result;
            }
			if (Get (0).GetType () == TokenType.WORD && Get (1).GetType () == TokenType.LPAREN) {
				return Function (parent);
			}
            if (Match(TokenType.WORD))
            {
                return new ConstantExpression(current.Text, parent);
            }

            if (Match(TokenType.TEXT))
            {
                return new StringExpression(current.Text);
            }
            throw new Exception("Unknown type of primary '"+current.Type+"'");
        }
        private Token Next()
        {
            Position++;
            if (Position >= Size) return EOF;
            return Tokens[Position];
        }
        // Проверяет, является ли ип токена равен аргументу
        private bool Match(TokenType type)
        {
            Token current = Get(0);
            if (current.GetType() != type)
                return false;
            Position++;
            return true;
        }
        // Проверяет валидность токена
        private Token Consume(TokenType type)
        {
            Token current = Get(0);
            if (current.GetType() != type)
                throw new Exception("Bad tokentype");
            Position++;
            return current;
        }
        // Получает токен со сдвигом
        private Token Get(int amount)
        {
            int finalPos = Position + amount;
            if (finalPos >= Size) return EOF;
            return Tokens[finalPos];
        }
    }
}
