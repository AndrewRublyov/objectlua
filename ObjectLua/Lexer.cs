﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua
{
    public class Lexer
    {
        private static string OPERATOR_CHARS = "+-*/()=<>!&|~.,^{}[]";
        private Dictionary<string, TokenType> OPERATORS = new Dictionary<string, TokenType>();

        private List<Token> Tokens;
        private string Source;
        private int Length;
        private int Position = 0;

        public Lexer(string chunk)
        {
            // Задаем операторы
            OPERATORS.Add("+", TokenType.PLUS);
            OPERATORS.Add("-", TokenType.MINUS);
            OPERATORS.Add("*", TokenType.STAR);
            OPERATORS.Add("/", TokenType.SLASH);
            OPERATORS.Add("^", TokenType.POW);
            OPERATORS.Add("(", TokenType.LPAREN);
            OPERATORS.Add(")", TokenType.RPAREN);
            OPERATORS.Add("=", TokenType.EQ);
            OPERATORS.Add("+=", TokenType.PLUSEQ);
            OPERATORS.Add("-=", TokenType.MINUSEQ);
            OPERATORS.Add("*=", TokenType.STAREQ);
            OPERATORS.Add("/=", TokenType.SLASHEQ);
            OPERATORS.Add("^=", TokenType.POWEQ);
            OPERATORS.Add("<", TokenType.LT);
            OPERATORS.Add(">", TokenType.GT);
            OPERATORS.Add("{", TokenType.LBRACE);
            OPERATORS.Add("}", TokenType.RBRACE);
            OPERATORS.Add("[", TokenType.LSBRACE);
            OPERATORS.Add("]", TokenType.RSBRACE);

            OPERATORS.Add("==", TokenType.EQEQ);
            OPERATORS.Add("~=", TokenType.EXCLEQ);
            OPERATORS.Add("<=", TokenType.LTEQ);
            OPERATORS.Add(">=", TokenType.GTEQ);

            OPERATORS.Add("~", TokenType.EXCL);

            OPERATORS.Add(".", TokenType.DOT);
            OPERATORS.Add("..", TokenType.DOTDOT);
            OPERATORS.Add(",", TokenType.COMMA);

            Source = chunk;
            Length = chunk.Length;

            Tokens = new List<Token>();
        }

        public List<Token> Tokenize()
        {
            while (Position < Length)
            {
                char curr = Peek(0);
                if (char.IsDigit(curr)) TokenizeNumber();
                else if (curr == '-' && Peek(1) == '-') //Пропускаем комментарии
                {
                    Next(); // Пропускаем первую -
                    Next(); // Пропускаем вторую -
                    if (Peek(0) != '[' && Peek(1) != '[')
                        while (curr != '\n' && curr != '\0')
                        {
                            curr = Next();
                        }
                    else
                        while (curr != ']' && Peek(-1) != ']')
                        {
                            curr = Next();
                        }
                }

                else if (char.IsLetter(curr)) TokenizeWord();
                else if (curr == '"' || curr == '\'') TokenizeText(curr);
                else if (OPERATOR_CHARS.IndexOf(curr) != -1)
                {
                    TokenizeOperator();
                }
                else //Друге символы
                {
                    Next();
                }
            }
            Source = "";
            return Tokens;
        }

        // Токенизация строк (c - символ начала строки " или ')
        private void TokenizeText(char c)
        {
            Next(); // Пропускаем кавычку

            string s = "";
            char current = Peek(0);
            while (true)
            {
                if (current == '\\' && (Peek(1) == '\'' || Peek(1) == '\"'))
                {
                    s += Peek(1);
                    Next();
                    current = Next();
                    continue;
                }
                if (current == c)
                {
                    Next(); // Пропускаем кавычку
                    break;
                }
                s += current;
                current = Next();
            }
            // Закончили считывать, создаем токен
            AddToken(TokenType.TEXT, s);
        }

        private void TokenizeWord()
        {
            // Считываем цифры, пока не встретим не цифру
            string s = "";
            char current = Peek(0);
            while (true)
            {
                if (!char.IsLetterOrDigit(current) && current != '_' && current != '[' && current != ']' && current != '.')
                {
                    break;
                }
                s += current;
                current = Next();
            }

            // Словесные операторы тут
            switch (s)
            {
                case "print": AddToken(TokenType.PRINT); return;
                case "if": AddToken(TokenType.IF); return;
                case "else": AddToken(TokenType.ELSE); return;
                case "and": AddToken(TokenType.AND); return;
                case "or": AddToken(TokenType.OR); return;
                case "then": AddToken(TokenType.THEN); return;
                case "do": AddToken(TokenType.DO); return;
                case "end": AddToken(TokenType.END); return;
                case "while": AddToken(TokenType.WHILE); return;
                case "for": AddToken(TokenType.FOR); return;
                case "break": AddToken(TokenType.BREAK); return;
                case "continue": AddToken(TokenType.CONTINUE); return;
                case "local": AddToken(TokenType.LOCAL); return;
                case "return": AddToken(TokenType.RETURN); return;
                case "function": AddToken(TokenType.FUNCTION); return;
                default: AddToken(TokenType.WORD, s); return;
            }

        }

        private void TokenizeOperator()
        {
            char current = Peek(0);
            string buff = "";
            while (true)
            {
                string text = buff;
                if (!OPERATORS.ContainsKey(text + current) && text != "")
                {
                    AddToken(OPERATORS[text]);
                    return;
                }
                buff += current;
                current = Next();
            }
        }

        private void TokenizeNumber()
        {
            // Считываем цифры, пока не встретим не цифру
            string s = "";
            char current = Peek(0);
            while (true)
            {
                // Поддержка чисел с плавающей точкой
                if (current == '.')
                {
                    if (s.IndexOf('.') != -1) throw new Exception("Invalid number");

                }
                else if (!char.IsDigit(current))
                {
                    break;
                }
                if (current == '.')
                    s += ',';
                else
                    s += Peek(0);

                current = Next();
            }
            // Закончили считывать, создаем токен
            AddToken(TokenType.NUMBER, s);
        }

        private void AddToken(TokenType type, string text = "")
        {
            Tokens.Add(new Token(type, text));
        } 

        private char Next()
        {
            Position++;
            return Peek(0);
        }

        private char Peek(int amount)
        {
            int finalPos = Position + amount;
            if (finalPos >= Length) return '\0';
            return Source[finalPos];
        }

        public override string ToString()
        {
            return string.Join("\n", Tokens);
        }

        //public void SetSource(string source)
        //{
        //    Source = source;
        //    Length = source.Length;
        //    //Tokens = new List<Token>();
        //}
    }
}