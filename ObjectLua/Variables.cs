﻿using ObjectLua.ValueTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua
{
    public class Variables
    {
        public Dictionary<string, Value> Vars = new Dictionary<string, Value>();

        public virtual void Set(string name, Value value, bool local)
        {
            if (!Vars.ContainsKey(name))
                Vars.Add(name, value);
            else
                Vars[name] = value;
        }

        public virtual Value Get(string name)
        {
            if (!Vars.ContainsKey(name)) { Console.WriteLine("Key {0} not found", name); return new NumberValue(0); }
            return Vars[name];
        }

        public bool Exists(string name)
        {
            if (Vars.ContainsKey(name)) return true;
            return false;
        }

        public void Delete(string name)
        {
            if (Vars.ContainsKey(name)) Vars.Remove(name);
        }
    }
}
