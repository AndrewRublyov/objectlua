﻿namespace ObjectLua
{
    public class Token
    {
        public TokenType Type;
        public string Text;

        public Token()
        {

        }

        public Token(TokenType type, string text)
        {
            Type = type;
            Text = text;
        }

        public void SetType(TokenType type)
        {
            Type = type;
        }
        public TokenType GetType()
        {
            return Type;
        }
        public void SetText(string text)
        {
            Text = text;
        }
        public string GetText()
        {
            return Text;
        }

        public override string ToString()
        {
            return Type + " " + Text;
        }
    }
}