﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua
{
    public enum TokenType
    {
        // Common

        WORD, 
        NUMBER, 
        TEXT,

        // Statements

        MINUS,          // -
        PLUS,           // +
        SLASH,          // /
        STAR,           // *
        RPAREN,         // )
        LPAREN,         // (
        POW,            // ^
        EXCL,           // ~
        EXCLEQ,         // ~=
        EQ,             // =
        PLUSEQ,         // +=
        MINUSEQ,        // -=
        STAREQ,         // *=
        SLASHEQ,        // /=
        POWEQ,          // ^=
        EQEQ,           // ==
        LT,             // <
        LTEQ,           // <=
        GT,             // >
        GTEQ,           // >=
        DOT,            // .
        DOTDOT,         // ..
        COMMA,          // ,
        LBRACE,         // {
        RBRACE,         // }
        LSBRACE,        // [
        RSBRACE,        // ]

        // Logical
        OR,             // or
        AND,            // and

        // Block bounds
        THEN,           // then
        DO,             // do
        END,            // end

        // Keywords

        PRINT,          // print
        IF,             // if
        ELSE,           // else
        WHILE,          // while
        FOR,            // for
        BREAK,          // break
        CONTINUE,       // continue
        LOCAL,          // local
        FUNCTION,       // function
        RETURN,         // return


        // Special

        EOF
    }
}
