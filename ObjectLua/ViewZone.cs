﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectLua.ValueTypes;

namespace ObjectLua
{
    public class ViewZone : Variables
    {
        private ViewZone parent;

        public ViewZone()
        {
        }

        public void SetParent(ViewZone parent)
        {
            this.parent = parent;
        }

        public ViewZone GetParent()
        {
            return parent;
        }

        public override Value Get(string name)
        {
            if (Exists(name))
            {
                //Console.WriteLine("'{0}' == {1}", name, base.Get(name));
                return base.Get(name);
            }
            if (parent == null)
            {
                //Console.WriteLine("'{0}' not found", name);
                return new NumberValue(0);
            }
            return parent.Get(name);
        }

        public override void Set(string name, Value value, bool local)
        {
            /*if (parent == null) Console.WriteLine("Parent is null");
            else Console.WriteLine("Parent isn't null");*/
            if (local || parent == null)
            {
                //Console.WriteLine("Set (local: {2}) {0} = {1}", name, value, local);
                base.Set(name, value, true);
                return;
            }
            if (Exists(name))
            {
                base.Set(name, value, local);
                return;
            }
            parent.Set(name, value, false);
        }
    }
}
