﻿using ObjectLua.ValueTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua.AST
{
    public class UnaryExpression : Expression
    {
        public enum Operator
        {
            PLUS,
            PLUSPLUS,
            MINUS,
            MINUSMINUS,
            NOT_EQUALS
        }
        private Expression exp;
        private Operator operation;

        public UnaryExpression(Expression expr, Operator oper)
        {
            exp = expr;
            operation = oper;
        }

        public Value Evaluate()
        {
            switch (operation)
            {
                case Operator.MINUS: return new NumberValue(exp.Evaluate().AsFloat() * -1);
                case Operator.MINUSMINUS: return new NumberValue(exp.Evaluate().AsFloat() - 1);
                case Operator.PLUS: return exp.Evaluate();
                case Operator.PLUSPLUS: return new NumberValue(exp.Evaluate().AsFloat() + 1);
                case Operator.NOT_EQUALS: return NotEqualsEval(exp);
                default: return exp.Evaluate();
            }
        }

        Value NotEqualsEval(Expression e)
        {
            Value result = e.Evaluate();
            if (result.AsFloat() == 0) //Если false - вернем true
                return new NumberValue(1);
            else  // Если строка или true - вернем 0 (строка это 1)
                return new NumberValue(0);
        }

        public override string ToString()
        {
            return operation + " " + exp;
        }
    }
}
