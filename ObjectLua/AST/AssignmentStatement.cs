﻿using ObjectLua.ValueTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua.AST
{
    public class AssignmentStatement : Statement
    {
        private string name;
        private Expression expression;
        private TokenType tokenType; // Тип для присваивания. Может быть = += -= *= /=
        private BlockStatement parent;
        private bool local;

        public AssignmentStatement(string name, Expression exp, TokenType tokenType, BlockStatement parent, bool local)
        {
            this.name = name;
            expression = exp;
            this.tokenType = tokenType;
            this.parent = parent;
            this.local = local;
            Name = "IF " + Machine.r.Next(10, 99);
        }

        public override void Execute()
        {
            Value result = expression.Evaluate();
            switch (tokenType)
            {
                case TokenType.EQ: parent.Set(name, result, local); break;
                case TokenType.PLUSEQ: result = new NumberValue(parent.Get(name).AsFloat() + result.AsFloat()), local); break;
                case TokenType.MINUSEQ: result = new NumberValue(parent.Get(name).AsFloat() - result.AsFloat()), local); break;
                case TokenType.STAREQ: result = new NumberValue(parent.Get(name).AsFloat() * result.AsFloat()), local); break;
                case TokenType.SLASHEQ: result = new NumberValue(parent.Get(name).AsFloat() / result.AsFloat()), local); break;
                case TokenType.POWEQ: result = new NumberValue((float)Math.Pow(parent.Get(name).AsFloat(), result.AsFloat())), local); break;
            }
            // TODO Впихнуть индексы
            parent.Set(name, result, local);
        }

        public Expression GetExpression()
        {
            return expression;
        }

        public string GetName()
        {
            return name;
        }

        public override string ToString()
        {
            return name + " = " + Machine.Globals.Get(name);
        }
    }
}
