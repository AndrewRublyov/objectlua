﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectLua.ValueTypes;

namespace ObjectLua.AST
{
    class UserFunctionStatement : BlockStatement
    {
        private string name;
        private List<string> parametersName;
        private BlockStatement body;
        private BlockStatement parent;
        private bool local;

        public UserFunctionStatement(string name, List<string> parametersName, BlockStatement body, BlockStatement parent, bool local)
        {
            Setup(name, parametersName, body, parent, local);
        }

        public UserFunctionStatement()
        {

        }

        public void Setup(string name, List<string> parametersName, BlockStatement body, BlockStatement parent, bool local)
        {
            this.parametersName = parametersName;
            this.body = body;
            this.parent = parent;
            this.name = name;
            this.local = local;
        }

        public override void Execute()
        {
            // TODO Добавить локальность
            parent.Set(name, new UserFunction(body, parametersName), local);
        }
    }
}
