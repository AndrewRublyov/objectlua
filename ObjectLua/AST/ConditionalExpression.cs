﻿using ObjectLua.ValueTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua.AST
{
    public class ConditionalExpression : Expression
    {
        public enum Operator
        {
            PLUS,
            MINUS,
            MULTIPLY,
            DIVIDE,

            EQUALS,
            NOT_EQUALS,

            LT,
            GT,
            LTEQ,
            GTEQ,

            AND,
            OR
        }
        private Expression exp1;
        private Expression exp2;
        private Operator operation;
        public ConditionalExpression(Expression expr1, Expression expr2, Operator oper)
        {
            exp1 = expr1;
            exp2 = expr2;
            operation = oper;
        }

        public Value Evaluate()
        {
            float number1, number2;
            float result;
            number1 = exp1.Evaluate().AsFloat();
            number2 = exp2.Evaluate().AsFloat();
            switch (operation)
            {
                case Operator.EQUALS: result = number1 == number2 ? 1 : 0; break;
                case Operator.GT: result = number1 > number2 ? 1 : 0; break;
                case Operator.GTEQ: result = number1 >= number2 ? 1 : 0; break;
                case Operator.LT: result = number1 < number2 ? 1 : 0; break;
                case Operator.LTEQ: result = number1 <= number2 ? 1 : 0; break;
                case Operator.NOT_EQUALS: result = number1 != number2 ? 1 : 0; break;
                case Operator.AND: result = (number1 != 0) && (number2 != 0) ? 1 : 0; break;
                case Operator.OR: result = (number1 != 0) || (number2 != 0) ? 1 : 0; break;
                default: result = number1 == number2 ? 1 : 0; break;
            }

            return new NumberValue(result);
        }

        public override string ToString()
        {
            return "["+exp1 + " " + operation.ToString() + " " + exp2+"]";
        }
    }
}
