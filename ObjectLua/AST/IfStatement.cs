﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua.AST
{
    class IfStatement : BlockStatement
    {
        private Expression expression;
        private Statement ifStatement;
        private Statement elseStatement;

        public IfStatement(Expression e, Statement ifSt, Statement elSt)
        {
            expression = e;
            ifStatement = ifSt;
            elseStatement = elSt;
        }

        public IfStatement()
        {
        }

        public void Setup(Expression e, Statement ifSt, Statement elSt)
        {
            expression = e;
            ifStatement = ifSt;
            elseStatement = elSt;
        }

        public override void Execute()
        {
            float result = expression.Evaluate().AsFloat();
            if (result != 0)
                ifStatement.Execute();
            else if (elseStatement != null)
                elseStatement.Execute();
        }
    }
}
