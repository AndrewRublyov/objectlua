﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectLua.ValueTypes;

namespace ObjectLua.AST
{
    public class BlockStatement : Statement
    {
        private List<Statement> statements;
        private ViewZone viewZone = new ViewZone();
        public BlockStatement()
        {
            statements = new List<Statement>();
        }

        public ViewZone GetView()
        {
            return viewZone;
        }

        public void SetView(ViewZone viewZone)
        {
            this.viewZone = viewZone;
        }

        public void AddStatement(Statement s)
        {
            statements.Add(s);
        }

        public override void Execute()
        {
            foreach (Statement s in statements)
            {
                s.Execute();
            }
        }

        public List<Statement> GetStatements()
        {
            return statements;
        }

        public Value Get(string name)
        {
            return viewZone.Get(name);
        }

        public void Set(string name, Value value, bool local)
        {
            viewZone.Set(name, value, local);
        }
    }
}
