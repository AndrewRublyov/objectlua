﻿using ObjectLua.ValueTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua.AST
{
    class NumberExpression : Expression
    {
        private float value;

        public NumberExpression(float value)
        {
            this.value = value;
        }

        public Value Evaluate()
        {
            return new NumberValue(value);
        }

        public override string ToString()
        {
            return value + "";
        }
    }
}
