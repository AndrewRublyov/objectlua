﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectLua.ValueTypes;

namespace ObjectLua.AST
{
    public interface IStatement
    {
        void Execute();
    }

    public class Statement : Exception, IStatement
    {
        private BlockStatement parent;
        public string Name;

        public BlockStatement GetParent()
        {
            return parent;
        }

        public void SetParent(BlockStatement parent)
        {
            this.parent = parent;
            (this as BlockStatement).GetView().SetParent(parent.GetView());
        }

        public virtual void Execute()
        {
            // Для переопределения
        }


    }
}
