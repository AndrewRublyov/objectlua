﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectLua.AST
{
    class ReturnStatement : Statement
    {
        public Expression Expression;

        public ReturnStatement(Expression e)
        {
            Expression = e;
        }

        public override void Execute()
        {
            throw this;
        }
    }
}
