﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectLua.ValueTypes;

namespace ObjectLua.AST
{
    class StringExpression : Expression
    {
        private string value;
        
        public StringExpression(string value)
        {
            this.value = value;
        }

        public Value Evaluate()
        {
            return new StringValue(value);
        }
    }
}
