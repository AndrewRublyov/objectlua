﻿using System;
using System.Collections.Generic;
using ObjectLua.ValueTypes;

namespace ObjectLua.AST
{
	public class FunctionExpression : Expression
	{
		private string name;
		private List<Expression> arguments = new List<Expression>();
	    private BlockStatement parent;

		public FunctionExpression (string name, List<Expression> arguments, BlockStatement parent) {
			this.name = name;
			this.arguments = arguments;
		    this.parent = parent;
		}
			
		public FunctionExpression (string name, BlockStatement parent) : this(name, new List<Expression>(), parent) {

		}

		public void AddArgument(Expression arg) {
			arguments.Add (arg);
		}

		public Value Evaluate () {
			var func = parent.Get(name);
		    //if (func is NumberValue) return func;
            if (func is UserFunction)
			    return (func as UserFunction).Execute(arguments.ToArray());
            return (func as Function).Execute(arguments.ToArray());

        }
    }
}

