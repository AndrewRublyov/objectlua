﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace ObjectLua.AST
{
    class WhileStatement : BlockStatement
    {
        private Statement statement;
        private Expression condition;
        private BlockStatement parent;

        public WhileStatement(Expression condition, Statement statement, BlockStatement parent)
        {
            Setup(condition, statement, parent);
        }

        public WhileStatement()
        {
            
        }

        public void Setup(Expression condition, Statement statement, BlockStatement parent)
        {
            this.statement = statement;
            this.condition = condition;
            this.parent = parent;
        }

        public override void Execute()
        {
            while (condition.Evaluate().AsFloat() != 0)
            {
                try
                {
                    statement.Execute();
                }
                catch (ContinueStatement)
                {
                    continue;
                }
                catch (BreakStatement)
                {
                    break;
                }
            }
        }
    }
}
