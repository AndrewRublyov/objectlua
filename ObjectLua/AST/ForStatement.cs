﻿using ObjectLua.ValueTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua.AST
{
    class ForStatement : BlockStatement
    {
        private AssignmentStatement assignmentstatement;
        private Expression to;
        private Expression step;
        private Statement statement;
        private BlockStatement parent;

        public ForStatement(AssignmentStatement assignmentstatement, Expression to, Expression step, Statement statement, BlockStatement parent)
        {
            Setup(assignmentstatement, to, step, statement, parent);
        }

        public ForStatement()
        {
        }

        public void Setup(AssignmentStatement assignmentstatement, Expression to, Expression step, Statement statement, BlockStatement parent)
        {
            this.assignmentstatement = assignmentstatement;
            this.to = to;
            this.step = step;
            this.statement = statement;
            Console.WriteLine("For " + to.Evaluate());
            this.parent = parent;
        }

        public override void Execute()
        {
            assignmentstatement.Execute();
            bool istrue;
            
            while (true)
            {
                if (step.Evaluate().AsFloat() > 0)
                    istrue = parent.Get(assignmentstatement.GetName()).AsFloat() <= to.Evaluate().AsFloat();
                else
                    istrue = parent.Get(assignmentstatement.GetName()).AsFloat() >= to.Evaluate().AsFloat();
                if (!istrue) break;
                try
                {
                    statement.Execute();
                }
                catch (ContinueStatement)
                {
                    continue;
                }
                catch (BreakStatement)
                {
                    break;
                }
                assignmentstatement = new AssignmentStatement(assignmentstatement.GetName(), new NumberExpression(assignmentstatement.GetExpression().Evaluate().AsFloat() + step.Evaluate().AsFloat()), TokenType.EQ, parent, true);
                assignmentstatement.Execute();
            }
            // Удаляем переменную цикла
            //Machine.Globals.Delete(assignmentstatement.GetName());
        }
    }
}
