﻿using ObjectLua.ValueTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua.AST
{
    class BinaryExpression : Expression
    {
        public enum Operator
        {
            PLUS,
            MINUS,
            STAR,
            SLASH,
            POW,
            DOTDOT
        }
        private Expression exp1;
        private Expression exp2;
        private Operator operation;
        public BinaryExpression(Expression expr1, Expression expr2, Operator oper)
        {
            exp1 = expr1;
            exp2 = expr2;
            operation = oper;
        }

        public Value Evaluate()
        {
            Value result1 = exp1.Evaluate();
            Value result2 = exp2.Evaluate();

            if (result1 is StringValue || result2 is StringValue)
            {
                switch (operation)
                {
                    case Operator.PLUS: return new StringValue(result1.AsString() + result2.AsString());
                    default: return new StringValue("");
                }
            }

            switch (operation)
            {
                case Operator.MINUS: return new NumberValue(result1.AsFloat() - result2.AsFloat());
                case Operator.PLUS: return new NumberValue(result1.AsFloat() + result2.AsFloat());
                case Operator.STAR: return new NumberValue(result1.AsFloat() * result2.AsFloat());
                case Operator.POW: return new NumberValue((float)Math.Pow(result1.AsFloat(), result2.AsFloat()));
                case Operator.SLASH: return new NumberValue(result1.AsFloat() / result2.AsFloat());
                default: return new NumberValue(result1.AsFloat() + result2.AsFloat());
            }
        }

        public override string ToString()
        {
            return "["+exp1 + " " + operation + " " + exp2+"]";
        }
    }
}
