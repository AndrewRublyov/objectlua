﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua.AST
{
    class FunctionStatement : Statement
    {
        private FunctionExpression expression;

        public FunctionStatement(FunctionExpression expr)
        {
            expression = expr;
        }

        public override void Execute()
        {
            expression.Evaluate();
        }
    }
}
