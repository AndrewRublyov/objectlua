﻿using ObjectLua.ValueTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua.AST
{
    public interface Expression
    {
        Value Evaluate();
    }
}
