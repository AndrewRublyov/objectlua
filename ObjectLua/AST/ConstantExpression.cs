﻿using ObjectLua.ValueTypes;
using ObjectLua;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua.AST
{
    class ConstantExpression : Expression
    {
        private string text;
        private BlockStatement parent;

        public ConstantExpression(string text, BlockStatement parent)
        {
            this.text = text;
            this.parent = parent;
        }

        public Value Evaluate()
        {
            return parent.Get(text);
        }
    }
}
