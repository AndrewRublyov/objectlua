﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua.AST
{
    public class BreakStatement : Statement
    {
        public override void Execute()
        {
            throw this;
        }
    }
}
