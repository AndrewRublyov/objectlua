﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectLua.AST
{
    class PrintStatement : Statement
    {
        private Expression expression;

        public PrintStatement(Expression expression)
        {
            this.expression = expression;
        }
        public override void Execute()
        {
            Console.WriteLine(expression.Evaluate());
        }
    }
}
