﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectLua.ValueTypes;

namespace ObjectLua.AST
{
    public class TableAssignmentStatement : Statement
    {
        private string name;
        private BlockStatement parent;
        private List<AssignmentStatement> items;

        public TableAssignmentStatement()
        {
            
        }

        public TableAssignmentStatement(string name, BlockStatement parent, List<AssignmentStatement> items)
        {
            Setup(name, parent, items);
        }

        public void Setup(string name, BlockStatement parent, List<AssignmentStatement> items)
        {
            this.parent = parent;
            this.items = items;
            this.name = name;
        }

        public override void Execute()
        {
            TableValue tv = new TableValue();
            foreach (var item in items)
            {
                tv.SetItem(item.GetName(), item.GetExpression().Evaluate());
            }
            parent.Set(name, tv, false);
        }
    }
}
